#!/bin/bash

apt update -y
apt upgrade -y

echo "[ -- Configuring DateTime with systemd]"
apt install systemd
timedatectl set-timezone America/Sao_Paulo

# If update needed
# sudo do-release-upgrade -d
# sudo reboot

echo "[ -- Installing Docker]"
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update -y
sudo apt install -y docker-ce docker-ce-cli containerd.io
sudo systemctl start docker
sudo systemctl enable docker
sudo docker -version

echo "[ -- Installing Docker Compose Services]"
sudo docker compose -f docker-compose.yml up -d

#sudo docker exec -t -i ubuntu-postgres-1 /bin/bash
#Conectar no postgres dentro do docker
#psql -h localhost -U postgres -p 5432

echo "[ -- Update Password Postgres]"
sudo -u postgres psql template1 -c "ALTER USER postgres with encrypted password 'postgres';"
sudo -u postgres psql template1 -c "CREATE DATABASE management;"

echo "[ -- Installing openjdk-11]"
apt-get install openjdk-17-jdk -y
echo "Verificar variável de ambiente java"

echo "[ -- Installing nodejs v20.14.0]"
sudo curl -sL https://deb.nodesource.com/setup_20.x | sudo -E bash -
apt install nodejs -y

echo "[ -- Updating NPM 10.8.1]"
npm install -g npm@10.8.1

echo "[ -- Add NPM angular cli package]"
npm i -g @angular/cling

echo "[ -- Installing NGINX v1.18.0]"
apt install nginx=1.24.* -y

echo "[ -- Criando diretórios de trabalho /opt/management]"
mkdir /opt/management
mkdir /opt/notification
mkdir /opt/frontend-angular
mkdir /opt/scripts

echo "[ -- Defining dir permissions]"
chmod -R 777 /opt/management/ /opt/notification /opt/frontend-angular /opt/scripts/

echo "[ -- Creating Daemon Manager Service in Ubuntu (/opt/scripts/daemon-service-manager.sh)]"
{
    echo '#!/bin/bash'
    echo 'SERVICE=$1;'
    echo 'SERVICE_STATUS=$(echo $(sudo systemctl is-active ${SERVICE}));'
    echo 'if [ "inactive" = "${SERVICE_STATUS}" ]; then'
    echo '  echo "[ -- Service ${SERVICE} is inactive, nothing to do];";'
    echo 'else {'
    echo '  sudo systemctl stop ${SERVICE};'
    echo '  echo "[ -- The Service ${SERVICE} were stopped!]";'
    echo '};'
    echo 'fi'
} >> /opt/scripts/daemon-service-manager.sh

echo "[ -- Creating Daemon for Management Service]"
{
    echo '[Unit]'
    echo 'Description=REST Spring Boot Management Service'
    echo 'After=syslog.target'
    echo '\n'
    echo '[Service]'
    echo 'User=ubuntu'
    echo 'WorkingDirectory=/opt/management'
    echo 'StandardOutput=journal'
    echo 'StandardError=journal'
    echo 'SyslogIdentifier=management'
    echo 'ExecStart=/usr/bin/java -jar /opt/management/management-0.0.1.jar'
    echo 'SuccessExitStatus=143'
    echo '\n'
    echo '[Install]'
    echo 'WantedBy=multi-user.target'
} >> /etc/systemd/system/management.service

echo "[ -- Creating Daemon for Notification Service]"
{
    echo '[Unit]'
    echo 'Description=REST Spring Boot Notification Service'
    echo 'After=syslog.target'
    echo '\n'
    echo '[Service]'
    echo 'User=ubuntu'
    echo 'WorkingDirectory=/opt/notification'
    echo 'StandardOutput=journal'
    echo 'StandardError=journal'
    echo 'SyslogIdentifier=notification'
    echo 'ExecStart=/usr/bin/java -jar /opt/notification/notification-0.0.1.jar'
    echo 'SuccessExitStatus=143'
    echo '\n'
    echo '[Install]'
    echo 'WantedBy=multi-user.target'
} >> /etc/systemd/system/notification.service

echo "[ -- Creating Daemon for Frontend Angular Service]"
{
    echo '[Unit]'
    echo 'Description=UI Service in Angular from Platform'
    echo 'After=syslog.target'
    echo '\n'
    echo '[Service]'
    echo 'User=ubuntu'
    echo 'WorkingDirectory=/opt/frontend-angular'
    echo 'StandardOutput=journal'
    echo 'StandardError=journal'
    echo 'SyslogIdentifier=frontend-angular'
    echo 'ExecStart=ng serve -o'
    echo 'SuccessExitStatus=143'
    echo '\n'
    echo '[Install]'
    echo 'WantedBy=multi-user.target'
} >> /etc/systemd/system/frontend-angular.service

echo "[ -- Creating reverse proxy file with NGINX]"
{
    echo 'server {'
    echo '  server_name application.management.com;'
    echo '  location / {'
    echo '      proxy_pass http://127.0.0.1:4200;'
    echo '  }'
    echo '  location /api/management/v1 {'
    echo '      proxy_pass http://127.0.0.1:8081;'
    echo '  }'
    echo '}'
} >> /etc/nginx/sites-available/management.com.conf

echo "[ -- Starting Daemons]"
systemctl enable management.service
systemctl daemon-reload

echo "[ -- Creationg Symbolic Links for Reverse Proxy]"
unlink /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/management.com.conf /etc/nginx/sites-enabled/management.com.conf
systemctl restart nginx

## Para ajustar o letscncrypt
## https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal