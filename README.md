# Management Service Application

## Documentação do Projeto

### Backend

### Visão Geral
O projeto consiste em dois serviços principais:
1. **Gestão de Dados de Usuários [management-service](https://gitlab.com/code-challenge-code-dev/management-service)**
2. **Envio de Notificações por Email via AWS SES [notification-service](https://gitlab.com/code-challenge-code-dev/notification-service)****
3. **[Frontend com Angular 17](https://gitlab.com/code-challenge-code-dev/frontend-angular)**

### Vizualização do sistema:

#### Home:
![imagem](/imgs/img_2.png)

#### Menu Lateral como lista dinâmica:
![imagem](/imgs/img_3.png)

#### Listagem e gestão das contas de usuários:
![imagem](/imgs/img_4.png)

#### Criação de um novo usuário:
![imagem](/imgs/img_5.png)
Para criar um novo usuário, o e-mail possui restrição de unicidade e deve ser um e-mail válido para recepção do email de boas vindas.

#### Email de boas vindas para o Usuário Matheus:
![imagem](/imgs/boas-vindas.png)

#### Alteração de um usuário existente com campos inválidos:
![imagem](/imgs/img_6.png)

#### Modal de Detalhamento de Usuário:
![imagem](/imgs/img_7.png)

#### Alteração de um usuário existente com campos válidos mas sem estarem alterados:
![imagem](/imgs/img_8.png)

#### Modal de confirmação de deleção:
![imagem](/imgs/img_9.png)

#### Resposta toaster após deleção do usuário da imagem anterior:
![imagem](/imgs/img_10.png)

#### Campo de busca:
- Para esse caso, somente após digitar 3 caracteres, uma requisição será efetuada no backend para que o filtro não impacte na performance das buscas.
- A busca também considera paginação.
![imagem](/imgs/img_11.png)

#### Campo de busca com resposta filtrada:
![imagem](/imgs/img_12.png)

### Arquitetura proposta

![Arquitetura](/imgs/img_1.png)


## Resumo do Projeto

O Management Service Application é um serviço construído para gerenciar dados de usuários e enviar notificações por email via AWS SES de maneira distribuída e escalável. O projeto foi desenvolvido utilizando uma stack moderna e robusta para garantir alta performance, escalabilidade e facilidade de manutenção. Abaixo, destacamos as principais tecnologias e o porquê de suas utilizações.

### Tecnologias Utilizadas

#### Spring Boot
- **Versão:** 3.2.5

#### Java
- **Versão:** 17

#### PostgreSQL
- **Versão:** 16.2 (via Docker Container)
- **Motivo:** Banco de dados relacional avançado, confiável e com suporte a diversas funcionalidades como transações ACID, integridade referencial e extensão de capacidades através de plugins.

#### Kafka
- **Motivo:** Plataforma de streaming distribuída, ideal para a construção de pipelines de dados em tempo real e sistemas de mensagens escaláveis.

#### Dependências Principais
- **Spring Boot Starter Actuator:** Fornece endpoints para monitoramento e gerenciamento da aplicação.
- **Spring Boot Starter Web:** Utilizado para criar APIs RESTful.
- **Spring Boot Starter Data JPA:** Facilita a integração com bancos de dados relacionais através do JPA.
- **Spring Boot Starter Validation:** Fornece funcionalidades de validação de dados.
- **Spring Kafka:** Integração com o Apache Kafka para sistemas de mensagens.

#### Dependências Adicionais
- **Spring Boot DevTools:** Melhora a experiência de desenvolvimento com reinicialização automática e outras ferramentas.
- **MapStruct:** Ferramenta para mapeamento de objetos, facilitando a conversão entre diferentes camadas da aplicação.
- **Lombok:** Reduz a verbosidade do código Java através da geração automática de getters, setters e outros métodos comuns.
- **Flyway:** Migração de banco de dados para gerenciamento da versão do esquema.
- **Springdoc OpenAPI:** Gera documentação interativa da API utilizando o padrão OpenAPI.
- **PostgreSQL Driver:** Permite a conexão e interação com o banco de dados PostgreSQL.

### Dependências de Teste
- **Spring Boot Starter Test:** Framework de testes integrado com suporte a diversos tipos de testes (unidade, integração).
- **Testcontainers:** Fornece ambientes de teste dinâmicos utilizando containers Docker.
- **Rest-Assured:** Facilita a escrita de testes para APIs REST.
- **Mockito:** Framework para criação de mocks em testes unitários.

### Build e Plugins

#### Maven Plugins
- **Spring Boot Maven Plugin:** Facilita a construção e execução de aplicações Spring Boot.
- **Maven Compiler Plugin:** Configura a compilação do projeto para utilizar o Java 17.
- **Jacoco Maven Plugin:** Gera relatórios de cobertura de código.
- **Maven Surefire Plugin:** Gerencia a execução de testes durante o build.

### Configuração de Ambiente AWS EC2

- O ambiente deve ser iniciado com o script 'prepare-ec2.sh' presente na raiz deste repositório.
- Este script Bash automatiza a configuração de um ambiente de desenvolvimento ou produção em um servidor Ubuntu. Ele realiza desde a atualização do sistema até a configuração de serviços e servidores necessários para executar aplicações web e serviços REST.

### Execução Local dos projetos backend

Para executar localmente um projeto Spring Boot, siga os passos abaixo, utilizando Markdown para organização:

**Clonar o repositório:**
Clone o repositório do projeto Spring Boot para o seu ambiente local usando Git, se ainda não tiver feito:

Vamos utilizar o management-service como exemplo, embora possa ser seguida a mesma lógica para o notification-service.

```bash
git clone https://gitlab.com/code-challenge-code-dev/management-service
cd management-service
mvn clean install
mvn spring-boot:run
```

Para executar os testes unitários, será necessário ter o docker instalado para que seja possível a inicialização do banco de dados de teste.
```bash
mvn test
```

### Frontend

Para inicializar o projeto de Frontend, deverá ser instalado: 

- Node: v20.14.0
- NPM: v10.8.1
- Angular CLI: Latest

### Dependências Principais:
- **@angular/animations:** Fornece suporte para animações em aplicativos Angular.
- **@angular/cdk:** Conjunto de ferramentas para desenvolvimento de componentes comuns de interface do usuário.
- **@angular/common, @angular/core, @angular/platform-browser, @angular/platform-browser-dynamic:** Pacotes essenciais do Angular para funcionalidades básicas, manipulação de DOM, e inicialização dinâmica de módulos.
- **@angular/forms:** Módulo para formulários reativos e template-driven.
- **@angular/material:** Componentes e estilos de material design para Angular.
- **@angular/router:** Roteamento para navegação entre páginas dentro do aplicativo.

### Outras Dependências:
- **rxjs:** Biblioteca para programação reativa em JavaScript.
- **tslib:** Biblioteca de suporte para funções TypeScript geradas.
- **zone.js:** Biblioteca para detecção de mudanças no ambiente de execução JavaScript.

### Dependências de Desenvolvimento:
- **@angular-devkit/build-angular, @angular/cli, @angular/compiler-cli:** Ferramentas e configurações para compilar e construir projetos Angular.
- **@types/jasmine:** Tipos TypeScript para Jasmine, um framework de teste para JavaScript.
- **jasmine-core:** Núcleo do framework de testes Jasmine.
- **karma, karma-chrome-launcher, karma-coverage, karma-jasmine, karma-jasmine-html-reporter:** Ferramentas para executar testes unitários em diferentes navegadores.
- **typescript:** Linguagem de programação TypeScript para desenvolvimento Angular.

```bash
sudo curl -sL https://deb.nodesource.com/setup_20.x | sudo -E bash -
apt install nodejs -y
npm install -g npm@10.8.1
npm i -g @angular/cling
```

Para inicializar o projeto, execute:
```bash
ng serve -o
```


Abaixo, segue os tópicos solicitados no desafio, junto com sua implementação:

# Processo seletivo desenvolvedor fullstack Java/Angular

##### Requisitos técnicos obrigatórios
- [x] Java
- [x] Spring Boot
- [X] API/Webservices REST
- [X] JUnit
- [X] Angular 17 / TypeScript
  - [x] Estilização
- [x] Banco de dados Postgres;
- [x] Docker
- [x] GitLab / CI/CD;
- [x] AWS (EC2 ou EKS)
- [x] Metodologias ágeis;

## > > Desafio: Sistema de Gerenciamento de Usuários < <

### Contexto - Geral
- [x] Este sistema deve permitir que os administradores realizem operações CRUD (Criar, Pesquisar, Atualizar e Deletar) usuários.

### Requisitos Técnicos - Específico

1. [x] **Java 17 ou superior**: O backend do sistema deve ser desenvolvido em Java, utilizando as funcionalidades mais recentes disponíveis.
2. [X] **Angular 17**: A interface de usuário deve ser construída utilizando Angular 17, com operações CRUD para gerenciar usuários.
3. [x] **JPA e Hibernate**: Utilize JPA (Java Persistence API) e Hibernate para mapeamento objeto-relacional e realizar operações de banco de dados.
4. [x] **Flyway**: Utilize o Flyway para gerenciar a migração do esquema do banco de dados. Certifique-se de que o esquema do banco de dados seja versionado e mantenha-se consistente com as alterações no código.
5. [x] **Spring Boot**: Utilize o Spring Boot para configurar e desenvolver o backend do sistema. Aproveite ao máximo as convenções e funcionalidades oferecidas pelo Spring Boot.
6. [x] **Controle de Exceções**: Implemente um controle de exceções para validar e tratar os dados fornecidos pelo usuário. Certifique-se de fornecer mensagens de erro significativas e tratamentos adequados para situações inesperadas.
7. [X] **Casos de Uso de Usuários**: Implemente os seguintes casos de uso relacionados a usuários:
    - [X] Backend: 
      - [X] Cadastro de novo usuário.
      - [X] Listagem de usuários cadastrados.
      - [X] Visualização detalhada de um usuário específico.
      - [X] Atualização dos dados de um usuário.
      - [X] Exclusão de um usuário.

   - [X] Frontend:
      - [X] Cadastro de novo usuário.
      - [X] Listagem de usuários cadastrados.
      - [X] Visualização detalhada de um usuário específico.
      - [X] Atualização dos dados de um usuário.
      - [X] Exclusão de um usuário.
      - [ ] Estilização de páginas.

8. [x] **Entidade Departamento**:
   - [X] Backend:
      - [X] Implemente uma entidade "Departamento" para separar os usuários no sistema por departamento. 
      - [X] Cada usuário deve ser associado a um único departamento.

   - [x] Frontend:
     - [x] Implemente uma entidade "Departamento" para separar os usuários no sistema por departamento.
     - [x] Cada usuário deve ser associado a um único departamento.

### Requisitos Adicionais e Uso de Outras Tecnologias
- [x] Lombok
- [x] Internationalization with MessageSource
- [x] Error Handler com ControllerAdvice
- [x] Utilização de Clean Architecture ou Ports And Adapters
- [x] Unit Tests
- [ ] Documentacao com https://app.diagrams.net/
- [x] Notification Service utilizando Kafka broker
- [x] Deploy na AWS com EC2

### Entrega

- [x] O código-fonte do projeto deve ser entregue em um repositório Git (por exemplo, GitHub, GitLab, Bitbucket) e enviado para o email codedevelop.contato@gmail.com, envie também nesse email o seu curículo atualizado.
- [x] É uma boa prática fazer commits de forma incremental durante o desenvolvimento, fornecendo uma narrativa clara da evolução do projeto.

### Critérios de Avaliação

1. **Funcionalidade Completa**: 
   - [x] O sistema deve funcionar conforme os requisitos especificados, permitindo que os usuários realizem todas as operações CRUD de forma eficiente.
2. **Qualidade do Código**: 
   - [x] O código deve ser limpo, modular e seguir as melhores práticas de desenvolvimento em:
     - [x] Java;
     - [x] Angular.
     - [x] Deve ser facilmente compreensível e passível de manutenção.
3. **Testabilidade**: 
   - [x] O código deve ser testável
   - [x] Testes unitários
   - [x] Testes de integração cobrindo as principais funcionalidades do sistema
4. **Documentação**: 
   - [ ] Forneça uma documentação clara e concisa
   - [ ] Descrevendo a arquitetura do sistema 
   - [ ] As tecnologias utilizadas
   - [ ] Instruções para configurar e executar o projeto localmente


